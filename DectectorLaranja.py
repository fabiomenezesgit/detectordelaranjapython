import numpy

import cv2
import numpy as np
import Request
import SerialConfig
import time

cam = cv2.VideoCapture(0)
kernel = numpy.ones((5, 5), numpy.uint8)
i = 1

request = Request.HttpApi()
enviaDados = SerialConfig.Serial()
while (True):
    ret, frame = cam.read()
    rangomax = numpy.array([50, 100, 255])  # B, G, R
    rangomin = numpy.array([0, 50, 100])

    mask = cv2.inRange(frame, rangomin, rangomax)
    # reduce the noise
    opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

    # desenha as linhas
    height = np.size(frame, 0)
    width = np.size(frame, 1)
    x, y, w, h = cv2.boundingRect(opening)
    cv2.line(frame, (0, height // 2), (width, height // 2), (255, 0, 0), 2)
    cv2.line(frame, (0, height // 4), (width, height // 4), (255, 0, 0), 2)

    # cv2.line(frame, (0, 0), (width, 320), (255, 0, 0), 2)
    # cv2.line(frame, (width//2, 0), (width//2, height), (255, 0, 0), 2)
    # cv2.line(frame, (width//4, 0), (width//4, height), (255, 0, 0), 2)

    # cv2.line(frame, (width//2, 0), (width//4, height), (255, 0, 0), 2)

    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 3)
    cv2.circle(frame, (x + w // 2, y + h // 2), 5, (0, 0, 255), -1)
    cv2.HoughCircles(frame, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0)
    quadrante1 = (height // 2) + width

    x2 = (x + w // 2)
    y2 = (y + h // 2)

    # -------------- PRIMEIRA LINHA ------------
    if ((x2 < 640) and (x2 > 427.6)) and ((y2 < 320) and (y2 > 213.4)):
        print("TERCEIRA LINHA ESQUERDA")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        # enviaDados.enviaArduino("PRIMEIRO QUADRANTE");

    if ((x2 < 427.6) and (x2 > 213.6)) and ((y2 < 320) and (y2 > 213.4)):
        print("PRIMEIRO MEIO")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        # enviaDados.enviaArduino("PRIMEIRO QUADRANTE");

    if ((x2 < 213.6) and (x2 > 0)) and ((y2 < 320) and (y2 > 213.4)):
        print("PRIMEIRO DIREITA")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        # enviaDados.enviaArduino("SEGUNDO QUADRANTE");

    # -------------- SEGUNDA LINHA ------------
    if ((x2 < 640) and (x2 > 427.6)) and ((y2 < 213.4) and (y2 > 106.7)):
        print("SEGUNDA LINHA ESQUERDA")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        # enviaDados.enviaArduino("PRIMEIRO QUADRANTE");

    if ((x2 < 427.6) and (x2 > 213.6)) and ((y2 < 213.4) and (y2 > 106.7)):
        print("######### MEIO ################")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        time.sleep(15)

        break
        # enviaDados.enviaArduino("PRIMEIRO QUADRANTE");

    if ((x2 < 213.6) and (x2 > 0)) and ((y2 < 213.4) and (y2 > 106.7)):
        print("TERCEIRA LINHA ESQUERDA")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        # enviaDados.enviaArduino("SEGUNDO QUADRANTE");

        # -------------- TERCEIRA LINHA ------------

    if ((x2 < 640) and (x2 > 427.6)) and ((y2 < 106.7) and (y2 > 0)):
        print("TERCEIRA LINHA -> ESQUERDA")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        # enviaDados.enviaArduino("PRIMEIRO QUADRANTE");

    if ((x2 < 427.6) and (x2 > 213.6)) and ((y2 < 106.7) and (y2 > 0)):
        print("TERCEIRA LINHA -> MEIO")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        # enviaDados.enviaArduino("PRIMEIRO QUADRANTE");

    if ((x2 < 213.6) and (x2 > 0)) and ((y2 < 106.7) and (y2 > 0)):
        print("TERCEIRA LINHA -> DIREITA")
        i = i + 1
        print("NUMERO DE VEZES I = " + str(i))
        # enviaDados.enviaArduino("SEGUNDO QUADRANTE");

    cv2.imshow('camera', frame)

    k = cv2.waitKey(1) & 0xFF

    if k == 27:
        break
