import requests


class HttpApi:
    def enviaRequest(self, quadrante):
        print("ENVIANDO REQUISIÇÃO AO BRAÇO ROBOTICO")
        r = requests.post('http://httpbin.org/post', data={'quadrante': quadrante})
        print(r.status_code)
        return r.status_code
